#!/usr/bin/env node

const puppeteer = require('puppeteer');
const credentials = require('./credentials');
const jsonfile = require('jsonfile');

const loginPageSelectors = {
  email: 'input#email',
  password: 'input#password',
  submit: 'input#login',
};

const evaluateRestaurantList = () => {
  const results = document.querySelector('#resultList');
  return Array.from(
    results.querySelectorAll('li.restaurant-wrapper.peyaCard')
  ).map(l => {
    const info = JSON.parse(l.dataset.info);
    return {
      ...info,
      url: l.dataset.url,
      logoUrl: l
        .querySelector('figure > .arrivalLogo img')
        .getAttribute('data-original'),
    };
  });
};

(async () => {
  console.log('# Start');
  //const browser = await puppeteer.launch({headless: false, slowMo: 50});
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Go to login page
  console.log('\n## Log in process');
  console.log('Go to login page');
  await page.goto('https://www.pedidosya.com.uy/login');

  // sign in
  console.log('Start sign in');
  await page.click(loginPageSelectors.email);
  await page.keyboard.type(credentials.email);
  await page.click(loginPageSelectors.password);
  await page.keyboard.type(credentials.password);
  await page.click(loginPageSelectors.submit);

  await page.waitForNavigation();

  // Click on search with the default address
  console.log('Start search');
  await page.click('#search');
  await page.waitForNavigation();

  // Get info of restaurants from the two first result pages
  console.log('\n## Restaurant info gathering');
  console.log('First page of results');
  const restaurantsInfoFirstPage = await page.evaluate(evaluateRestaurantList);
  await Promise.all([
    page.waitForNavigation(),
    page.click('footer .pagination .current + li a'),
  ]);
  console.log('Second page of results');
  const restaurantsInfoSecondPage = await page.evaluate(evaluateRestaurantList);
  const restaurantsInfo = [
    ...restaurantsInfoFirstPage,
    ...restaurantsInfoSecondPage,
  ];

  console.log('Writing ./src/data/restaurants.json');
  jsonfile.writeFile(
    './src/data/restaurants.json',
    restaurantsInfo,
    { spaces: 2 },
    err => (err !== null ? console.log(err) : 0)
  );

  // Get info on all the dishes
  console.log('\n## Dishes info gathering');

  let allDishes = [];

  for (const restaurant of restaurantsInfo.filter(
    restaurant => restaurant.status === 'OPEN'
  )) {
    console.log(restaurant.name);
    await page.goto(restaurant.url);
    const newDishes = await page.evaluate(
      restaurantId =>
        Array.from(document.querySelectorAll('li.product')).map(el => {
          const name = el.querySelector('.productName');
          const description = el.querySelector('p');
          const price = el.querySelector('.price > span');
          const image = el.querySelector('.profile-image-wrapper img');
          return {
            id: el.dataset.id,
            name: name ? name.textContent.trim() : 'No name found : ERROR',
            description: description
              ? description.textContent.trim()
              : 'No description found : ERROR',
            price: parseFloat(
              price ? price.textContent.replace(/\$*/g, '') : '0'
            ),
            imageUrl: image ? image.getAttribute('src') : '',
            restaurantId,
          };
        }),
      restaurant.id
    );

    allDishes = [...allDishes, ...newDishes];
  }

  console.log('Writing ./src/data/dishes.json');
  jsonfile.writeFile(
    './src/data/dishes.json',
    allDishes,
    { spaces: 2 },
    err => (err !== null ? console.log(err) : 0)
  );
  console.info('\nDone!');

  await browser.close();
})();
