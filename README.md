# Pedidos Eventualmente

Query a food delivery site to get potential dishes to eat based on the price (accounting for tip).

## Installation

```
git clone git@bitbucket.org:DrummerHead/pedidoseventualmente.git
cd pedidoseventualmente/
yarn install
```

## Configuration

Edit `config.js` and edit the desired price goals and tip range.

Copy `credentials.example.js` to `credentials.js` and put your email and password associated with your account.

On the side of the food delivery app, make sure that the default address that is selected after login (in the search landing page) is the one you want to use.


## Running

Run

```
./getData.js
```

To Query the site and get dish and restaurant information.

Then run the site with:

```
yarn start
```

and check `http://localhost:8080`

Keep in mind this data will change constantly, so every time you want to make a decision on what to eat, repeat these steps (`./getData.js && yarn start`)
