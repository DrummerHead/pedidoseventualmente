import { h, Component } from 'preact';
import config from '../../config';
import processedDishData from '../data/processedDishData';
import Dish from './Dish';

//function shuffleArray(array) {
//  const newArray = Array.from(array);
//  for (let i = newArray.length - 1; i > 0; i--) {
//    let j = Math.floor(Math.random() * (i + 1));
//    [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
//  }
//  return newArray;
//}

class Dishes extends Component {
  state = {
    filter: true,
    searchTerm: '',
    selectedDishes: [],
  };

  toggleFilter = ev => {
    this.setState(prevState => ({
      filter: !prevState.filter,
    }));
  };

  search = ev => {
    this.setState({
      searchTerm: ev.target.value,
    });
  };

  selectDish = dish => ev => {
    console.log('clicked this mofoko');
    this.setState(prevState => ({
      selectedDishes: [
        ...prevState.selectedDishes,
        processedDishData.find(d => d.id === dish.id),
      ],
    }));
  };

  render() {
    return (
      <main>
        <header className="control">
          <label>
            <input
              type="checkbox"
              onChange={this.toggleFilter}
              checked={this.state.filter}
            />{' '}
            Filter by goal price
          </label>
          <label>
            <input type="text" onInput={this.search} /> Search
          </label>
        </header>
        <div className="table">
          <div className="dishColumn">
            {processedDishData
              .filter(
                this.state.filter
                  ? dish =>
                      dish.goalPrice !== null &&
                      dish.openRestaurant &&
                      dish.shippingAmount <= config.maxShippingCost
                  : dish => true
              )
              .filter(dish =>
                dish.name
                  .toLowerCase()
                  .includes(this.state.searchTerm.toLowerCase())
              )
              .sort(
                (a, b) =>
                  parseFloat(b.restaurantRating) -
                  parseFloat(a.restaurantRating)
              )
              .map(dish => <Dish onClick={this.selectDish(dish)} {...dish} />)}
          </div>
          <div className="dishColumn dishColumn--fixed">
            {this.state.selectedDishes.map(dish => <Dish {...dish} />)}
          </div>
        </div>
      </main>
    );
  }
}

export default Dishes;
