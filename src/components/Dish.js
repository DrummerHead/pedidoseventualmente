import { h, Component } from 'preact';
import { findRestaurantById } from '../data/processedDishData';

const Dish = props => {
  const restaurant = findRestaurantById(props.restaurantId);
  return (
    <div className="dish" onClick={props.onClick}>
      <div className="dish__text">
        <div className="dish__restaurant">
          <a href={restaurant.url} target="_blank">
            <img
              src={restaurant.logoUrl}
              className="dish__logo"
              alt={restaurant.name}
              title={restaurant.name}
            />
          </a>
          <span className="dish__score">{props.restaurantRating}</span>
          <span className="dish__deliverytime">{restaurant.deliveryTime}</span>
        </div>
        <div className="dish__desc">
          <strong>{props.name}</strong>
          <p>{props.description}</p>
        </div>
        <div className="dish__prices">
          <span className="dish__goalprice">{props.goalPrice}</span>
          <span className="dish__price">{props.price}</span>
          <span className="dish__tip">
            {props.goalPrice - props.price - props.shippingAmount}
          </span>
          <span className="dish__shipping">{props.shippingAmount}</span>
        </div>
      </div>
      {props.imageUrl !== '' ? (
        <div className="dish__image">
          <img src={props.imageUrl} alt={props.name} />
        </div>
      ) : null}
    </div>
  );
};

export default Dish;
