import restaurants from '../data/restaurants.json';
import dishes from '../data/dishes.json';
import config from '../../config';

const getRestaurantsWithFixedScore = restaurants => {
  const averageScore =
    parseInt(
      restaurants.reduce((acc, curr) => curr.ratingScore + acc, 0) /
        restaurants.length *
        10,
      10
    ) / 10;
  return restaurants.map(
    res =>
      res.validReviewsCount < 50 ? { ...res, ratingScore: averageScore } : res
  );
};

const restaurantsWithFixedScore = getRestaurantsWithFixedScore(restaurants);

export const findRestaurantById = id =>
  restaurantsWithFixedScore.find(restaurant => restaurant.id === id);

const moneyStringToFloat = s =>
  s === null ? null : parseFloat(s.replace(/\D*/, ''));

const isDishWithinGoalPrices = (dish, shippingAmount) =>
  config.wannaSpend
    .map(goalPrice => [
      goalPrice,
      shippingAmount === null
        ? dish.price <= goalPrice - config.minTip &&
          dish.price >= goalPrice - config.maxTip
        : dish.price <= goalPrice - shippingAmount &&
          dish.price >= goalPrice - shippingAmount - config.minTip,
    ])
    .reduce((acc, curr) => (curr[1] ? curr[0] : acc), null);

const processedDishData = dishes.map(dish => {
  const restaurant = findRestaurantById(dish.restaurantId);
  const shippingAmount = moneyStringToFloat(restaurant.shippingAmount);
  return {
    ...dish,
    shippingAmount,
    openRestaurant: restaurant.status === 'OPEN',
    restaurantRating: restaurant.ratingScore,
    goalPrice: isDishWithinGoalPrices(dish, shippingAmount),
  };
});

export default processedDishData;
