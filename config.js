export default {
  wannaSpend: [200, 300, 400, 500],
  maxTip: 24,
  minTip: 15,
  maxShippingCost: 25,
};
